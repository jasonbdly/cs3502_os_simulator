using System;

namespace OS_Project_Phase_1 {
    public class CPU {
        public class InvalidRegisterException : Exception {
            public InvalidRegisterException(int registerNum) : base("Invalid register: " + registerNum) {}
        }

        private OS_Simulator os;

        private int[] registers;

        public CPU(OS_Simulator os) {
            this.os = os;
            registers = new int[NUMBER_OF_REGISTERS];
            for (int i = 0; i < registers.Length; i++) {
                registers[i] = 0;
            }
        }

        public void Start() {

        }

        public void SetRegister(int registerNum, int val) {
            if (registerNum > 0 && registerNum < registers.Length) {
                this.registers[registerNum] = val;
            }
            throw new InvalidRegisterException(registerNum);
        }

        public int GetRegister(int registerNum) {
            if (registerNum > 0 && registerNum < registers.Length) {
                return this.registers[registerNum];
            }
            throw new InvalidRegisterException(registerNum);
        }

        public void SetAccumulator(int val) {
            this.registers[REG_ACCUMULATOR] = val;
        }

        public int GetAccumulator() {
            return this.registers[REG_ACCUMULATOR];
        }

        public const int NUMBER_OF_REGISTERS = 16;
        public const int REG_ACCUMULATOR = 0,
            REG_ZERO = 1;
    }
}