using System;
using System.Collections.Generic;
using System.Globalization;

namespace OS_Project_Phase_1 {

    public class Instruction {
        public enum OPCODE {
            RD, WR,
            ST, LD,
            MOV,
            ADD, SUB, MUl, DIV,
            AND, OR,
            MOVI,
            ADDI, MULI, DIVI,
            LDI, SLT, SLTI,
            HLT, NOP, JMP,
            BEQ, BNE, BEZ, BNZ, BGZ, BLZ
        }

        public enum TYPE {
            ARITH,
            COND_IMMED,
            UNCOND_JUMP,
            IO
        }

        public OPCODE opcode {get; set;}
        public TYPE type {get; set;}
        public uint sourceRegister1 {get; set;}
        public uint sourceRegister2 {get; set;}
        public uint destinationRegister {get; set;}
        public uint baseRegister {get; set;}
        public uint address {get; set;}

        public Instruction(TYPE type, OPCODE opcode) {
            this.opcode = opcode;
            this.type = type;
        }

        static Instruction() {
            //Key all OPCODE values by index
            opcodes = new Dictionary<uint, OPCODE>();
            string[] opcodesRAW = Enum.GetNames(typeof(OPCODE));
            for (uint i = 0; i < opcodesRAW.Length; i++) {
                opcodes.Add(i, (OPCODE)Enum.Parse(typeof(OPCODE), opcodesRAW[i]));
            }

            //Key all TYPE values by index
            types = new Dictionary<uint, TYPE>();
            string[] typesRAW = Enum.GetNames(typeof(TYPE));
            for (uint i = 0; i < typesRAW.Length; i++) {
                types.Add(i, (TYPE)Enum.Parse(typeof(TYPE), typesRAW[i]));
            }

            //Create lambda expressions for each OPCODE, to be called in the future as needed
            operations = new Dictionary<OPCODE, Action<OS_Simulator>> {
                //READ
                [OPCODE.RD] = (os) => {
                    
                },

                //WRITE
                [OPCODE.WR] = (os) => {},

                //STORE
                [OPCODE.ST] = (os) => {},

                //LOAD
                [OPCODE.LD] = (os) => {},

                //MOVE
                [OPCODE.MOV] = (os) => {},

                //ADD
                [OPCODE.ADD] = (os) => {},

                //SUBTRACT
                [OPCODE.SUB] = (os) => {},

                //MULTIPLY
                [OPCODE.MUl] = (os) => {},

                //DIVIDE
                [OPCODE.DIV] = (os) => {},

                //AND
                [OPCODE.AND] = (os) => {},

                //OR
                [OPCODE.OR] = (os) => {},

                //IMMEDIATE MOVE
                [OPCODE.MOVI] = (os) => {},

                //IMMEDIATE ADD
                [OPCODE.ADDI] = (os) => {},

                //IMMEDIATE MULTIPLY
                [OPCODE.MULI] = (os) => {},

                //IMMEDIATE DIVIDE
                [OPCODE.DIVI] = (os) => {},

                //IMMEDIATE LOAD
                [OPCODE.LDI] = (os) => {},

                //STRICTLY LESS THAN
                [OPCODE.SLT] = (os) => {},

                //IMMEDIATE STRICTLY LESS THAN
                [OPCODE.SLTI] = (os) => {},

                //HALT
                [OPCODE.HLT] = (os) => {},

                //NO OP
                [OPCODE.NOP] = (os) => {},

                //JUMP
                [OPCODE.JMP] = (os) => {},

                //BRANCH IF EQUAL
                [OPCODE.BEQ] = (os) => {},

                //BRANCH IF NOT EQUAL
                [OPCODE.BNE] = (os) => {},

                //BRANCH IF EQUAL TO 0
                [OPCODE.BEZ] = (os) => {},
                
                //BRANCH IF NOT EQUAL TO 0
                [OPCODE.BNZ] = (os) => {},

                //BRANCH IF GREATER THAN 0,
                [OPCODE.BGZ] = (os) => {},

                //BRANCH IF LESS THAN 0
                [OPCODE.BLZ] = (os) => {}
            };
        }

        private static Dictionary<OPCODE, Action<OS_Simulator>> operations;
        private static Dictionary<uint, OPCODE> opcodes;
        private static Dictionary<uint, TYPE> types;

        public static void Execute(OS_Simulator os, Instruction instruction) {
            Action<OS_Simulator> operation = operations[instruction.opcode];
            operation(os);
        }

        public override string ToString() {
            return String.Format("op: {0}, type: {1}", opcode, type);
        }

        private static uint 
            MASK_TYPE           = Convert.ToUInt32("11000000000000000000000000000000", 2),
            MASK_OPCODE         = Convert.ToUInt32("00111111000000000000000000000000", 2),

            MASK_ARITH_SREG1    = Convert.ToUInt32("00000000111100000000000000000000", 2),
            MASK_ARITH_SREG2    = Convert.ToUInt32("00000000000011110000000000000000", 2),
            MASK_ARITH_DREG     = Convert.ToUInt32("00000000000000001111000000000000", 2),

            MASK_CI_BREG        = Convert.ToUInt32("00000000111100000000000000000000", 2),
            MASK_CI_DREG        = Convert.ToUInt32("00000000000011110000000000000000", 2),
            MASK_CI_ADDRESS     = Convert.ToUInt32("00000000000000001111111111111111", 2),

            MASK_JU_ADDRESS     = Convert.ToUInt32("00000000111111111111111111111111", 2),

            MASK_IO_REG1        = Convert.ToUInt32("00000000111100000000000000000000", 2),
            MASK_IO_REG2        = Convert.ToUInt32("00000000000011110000000000000000", 2),
            MASK_IO_ADDRESS     = Convert.ToUInt32("00000000000000001111111111111111", 2);

        public static Instruction Decode(string hexInstruction) {
            if (hexInstruction.StartsWith("0x")) {
                hexInstruction = hexInstruction.Substring(2);
            }

            //uint asBytes = UInt32.Parse(hexInstruction, NumberStyles.HexNumber);
            uint asBytes = UInt32.Parse(hexInstruction, NumberStyles.HexNumber);

            //Right bit shift 30 places to contain just the first two bits
            uint instructionType = (asBytes & MASK_TYPE) >> 30;

            //Shift left 2 to trim instruction type, then shift right 26 bits
            uint opcode = (asBytes & MASK_OPCODE) >> 24;

            //Create new Instruction instance to be populated
            Instruction instruction = new Instruction(types[instructionType], opcodes[opcode]);

            //Retrieve arguments from the instruction by applying the
            //appropriate pre-defined bit mask for each parameter.
            switch (instructionType) {
                //0 == binary<00>
                //Must be an arithmetic or register transfer instruction
                case 0:
                    instruction.sourceRegister1 = (asBytes & MASK_ARITH_SREG1) >> 20;
                    instruction.sourceRegister2 = (asBytes & MASK_ARITH_SREG2) >> 16;
                    instruction.destinationRegister = (asBytes & MASK_ARITH_DREG) >> 12;
                    break;
                
                //1 == binary<01>
                //Must be a conditional branch or an immediate instruction
                case 1:
                    instruction.baseRegister = (asBytes & MASK_CI_BREG) >> 20;
                    instruction.destinationRegister = (asBytes & MASK_CI_DREG) >> 16;
                    instruction.address = (asBytes & MASK_CI_ADDRESS) >> 12;
                    break;
                
                //2 == binary<10>
                //Must be an unconditional jump instruction
                case 2:
                    instruction.address = asBytes & MASK_JU_ADDRESS;
                    break;

                //3 == binary<11>
                //Must be an IO instruction
                case 3:
                    instruction.sourceRegister1 = (asBytes & MASK_IO_REG1) >> 20;
                    instruction.sourceRegister2 = (asBytes & MASK_IO_REG2) >> 16;
                    instruction.address = asBytes & MASK_IO_ADDRESS;
                    break;
            }

            return instruction;
        }
    }
}