using System;
using System.Collections.Generic;

namespace OS_Project_Phase_1 {
    public class DiskManager {
        private OS_Simulator os;
        private uint[] diskStorage;
        private int blockSize, numBlocks;

        private List<FileBlock> files;

        public DiskManager(OS_Simulator os) {
            this.os = os;
            this.blockSize = this.os.controlBlock.blockSize;
            this.numBlocks = this.os.controlBlock.totalMemory / this.blockSize;
            this.diskStorage = new uint[this.os.controlBlock.totalMemory];
            files = new List<FileBlock>();
            init();
        }

        private void init() {
            Wipe();
        }

        private FileBlock getFileBlock(int fileId) {
            if (fileId > -1 && fileId < files.Count) {
                return files[fileId];
            } else {
                throw new FileException("Invalid file Id: " + fileId);
            }
        }

        private static void verifyFileLocation(FileBlock file, int location) {
            if (location < 0 || location >= file.size) {
                throw new FileException("Invalid file location: " + location);
            }
        }

        private static void verifyFileRange(FileBlock file, int startLocation, int endLocation) {
            verifyFileLocation(file, startLocation);
            verifyFileLocation(file, endLocation);
            if (endLocation <= startLocation) {
                throw new FileException("End of file range must be greater than the start of the file range: [" + startLocation + "," + endLocation + "]");
            }
        }

        private static void verifyFileRange(FileBlock file, int startLocation, int endLocation, uint[] values) {
            verifyFileRange(file, startLocation, endLocation);
            if (values.Length != (endLocation - startLocation)) {
                throw new FileException("Length of write data must match length of write range: " + values.Length + " != " + (endLocation - startLocation));
            }
        }

        public int CreateFile() {
            //Calculate the id of the new file
            int fileId = files.Count;
            
            if (fileId < numBlocks) {
                //Create the new file
                FileBlock file = new FileBlock();

                //Populate the newly created file's attributes
                file.id = fileId;
                file.startLocation = fileId * blockSize;
                file.size = blockSize;

                //Register the newly created file
                files.Add(file);

                //Return the new file's Id for external retrieval
                return fileId;
            } else {
                throw new FileException("Unable to create new file. Disk full.");
            }
        }

        public void Write(int fileId, int location, uint data) {
            //Retrive the specified file block
            FileBlock file = getFileBlock(fileId);

            //Verify that the write location is valid
            verifyFileLocation(file, location);

            //Calculate the disk write location
            int diskLocation = file.startLocation + location;

            //Store the supplied data to disk at the specified location
            diskStorage[diskLocation] = data;
        }

        public void Write(int fileId, int startLocation, int endLocation, uint[] data) {
            //Retrieve the specified file block
            FileBlock file = getFileBlock(fileId);

            //Verify that the write range is valid
            verifyFileRange(file, startLocation, endLocation, data);
            
            //Calculate the start and end write positions relative to disk
            int diskStart = file.startLocation + startLocation,
                diskEnd = file.startLocation + endLocation;

            //Write submitted data to disk
            for (int writeLocation = diskStart; writeLocation < diskEnd; writeLocation++) {
                diskStorage[writeLocation] = data[writeLocation - diskStart];
            }
        }

        public uint Read(int fileId, int location) {
            //Retreive the specified file block
            FileBlock file = getFileBlock(fileId);

            //Verify that the read location is valid
            verifyFileLocation(file, location);

            //Calculate the disk read location
            int diskLocation = file.startLocation + location;

            //Return the data at the specified location
            return diskStorage[diskLocation];
        }

        public uint[] Read(int fileId, int startLocation, int endLocation) {
            //Retrieve FileBlock for the specified file
            FileBlock file = getFileBlock(fileId);
            
            //Verify that the specified read range is valid
            verifyFileRange(file, startLocation, endLocation);

            //Calculate the read start/end locations relative to the disk
            int diskStart = file.startLocation + startLocation,
                diskEnd = file.startLocation + endLocation;
            
            //Create container for all read data
            uint[] readData = new uint[endLocation - startLocation];
            
            //Retrieve all data in the specified read range, and return
            for (int readLocation = diskStart; readLocation < diskEnd; readLocation++) {
                readData[readLocation - diskStart] = diskStorage[readLocation];
            }
            return readData;
        }

        public void Wipe() {
            //Write over all storage with 0s
            for (int i = 0; i < diskStorage.Length; i++) {
                diskStorage[i] = 0;
            }
        }

        private class FileBlock {
            public int id, startLocation, size;
        }

        public class FileException : Exception {
            public FileException(string message) : base("File Exception: " + message) {}
        }

        public class WriteException : Exception {
            public WriteException(string message) : base("Write Exception: " + message) {}
        }

        public class ReadException : Exception {
            public ReadException(string message) : base("Read Exception: " + message) {}
        }
    }
}